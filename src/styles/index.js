import styled from 'styled-components';

export const TextWrapper = styled.View`
    flex-direction: row;
`;

export const Wrapper = styled.View`
  flex: 1;  
  justify-content: center;
  align-items: center;  
  display: flex;
`;

export const RNButton = styled.Button`
  margin-left: -25px;
`;

export const OrderWrapper = styled.View`
  justify-content: flex-start;
  align-self: flex-start;

`;


export const Card = styled.View`
  flex: 1;  
  background-color: white;
  margin: 10px;
  margin-top: 10px;
  margin-bottom: 10px;
  border-width: 0.5px;
  border-color: black;
  border-radius: 8px;
  padding: 2px;
`;

export const ScrollWrapper = styled.ScrollView`
  flex: 1;  
`;

export const EndWrapper = styled(Wrapper)`
  justify-content: flex-end;
`;

export const TextField = styled.TextInput`    
  flex: 1;
  border: 0.5px;
  border-top-width: 0px;
  border-right-width: 0px;
  border-left-width: 0px;
  border-bottom-color: lightgray;  
`;

export const ForgotButton = styled.TouchableHighlight`
  background-color: transparent;  
  margin: 10px;
`;

export const Pick = styled.Picker`
  flex: 1;
  height: 50px;
  width: 100%;
`;

export const UserTypeSwitch = styled.Switch`
  flex: 1;
`;

export const Logo = styled.Image`
  height: 160px; 
  width: 180px;              
  padding: 25px;
  margin-bottom: 35px;                  
  
`;

export const TextShowItem = styled.Text`
                            
`;


export const TextShowTitle = styled.Text` 
  color: blue;
  margin-bottom: 5px;                            
`;

export const ItemWrapper = styled.View`
  flex: 1;  
  background-color: white;
  border-width: 0.8;
  border-color: gray;
  margin: 10px;
  padding: 10px;
  flex-direction: row;
`;

export const Photo = styled.Image`
  height: 100px;
  width: 100px;
`;

export const Label = styled.Text`
  font-size: 13px;
  color: blue;
`
export const ListSeparator = styled.View`
  height: 0.7px;
  background-color: gray;
`

export const ProductImage = styled.Image`
  height: 50px;
  width: 50px;  
`


export const SearchImage = styled.Image`
  height: 40px;
  width: 40px;  
`

export const ListItem = styled.View`
  display: flex;
  flex: 1;
  padding: 5px;
  flex-direction: row;
`
export const ProductDetails = styled.View`
  display: flex;
  flex-direction: column;
  margin-left: 5px;
  flex: 1;
`

export const LoadContainer = styled.View`
  background-color: transparent;
  position: absolute;
  flex: 1;
  display: flex;
  height: 100%;
  width: 100%;      
  justify-content: center;
  align-items: center;
  z-index: 2;
`;