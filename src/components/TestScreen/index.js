import React, { Component } from 'react';
import axios from 'axios';
import cepPromise from 'cep-promise';
import { TextWrapper, TextField, Pick, Wrapper } from '@Styles';
import Button from '../Button';
import Background from '../Background';
import WithFeedBack from '../WithFeedback';
import string from '@Strings';

class TestScreen extends Component {
    state = {
        total: '', 
        status: '', 
        criado_em: '', 
        cliente_id: '', 
        cadastrante_id: '', 
        forma_pagamento:'', 
        data_prevista:'', 
        data_entrega:'',
        id: '',
        quantidade: '',
        valor_unitario: '3.50',
        desconto: '0.00',
        produto_id: 0,
        pedido_id: 0,
        response: [],
        cep: '',
    };

    changeState = (field, value) => {
        this.setState({ [field]: value });
    };

    buscaCep = () => {
        const {
          cep,

        } = this.state;
        const { setLoading, showToast } = this.props;

        const valCep = /^[0-9]{2}[0-9]{3}[0-9]{3}$/;

        if (valCep) {
            setLoading(true);
            console.log(this.state.cep)
            cepPromise(`${this.state.cep}`)
            .then(response => {
                this.setState({
                    response: response,
                    state: response.state,
                    city: response.city,
                    neighborhood: response.neighborhood,
                    street: response.street
                })
                console.log(this.state.response)
                console.log(this.state.state)
                console.log(this.state.city)
                console.log(this.state.neighborhood)
                console.log(this.state.street)

            })
            .then(() => setLoading(false))
            

        } else {
            showToast({ message: string.Login.insertFields, type: `error` });
            console.log(this.state.cep)
            
        }
    };



  

    render() {
        const {
           total,
           quantidade,
           cep,
        } = this.state;
        const { navigation } = this.props;
        return (
            <Background>
                <TextWrapper>
                    <TextField
                        placeholder={string.Order.total}
                        value={this.state.cep}
                        keyboardType={'numeric'}
                        onChangeText={text => this.changeState('cep', text)}
                    />
                </TextWrapper>

                <Wrapper>
                    <Button
                        primary
                        title={string.Products.save}
                        onPress={() => this.buscaCep()}
                    />
                </Wrapper>

                 <TextWrapper>
                    <TextField
                        placeholder={string.Order.amount}
                        value={quantidade}
                        keyboardType={'numeric'}
                        onChangeText={text => this.changeState('quantidade', text)}
                    />
                </TextWrapper>

                <Wrapper>
                    <Button
                        primary
                        title={string.Products.save}
                        onPress={() => this.createOrderItem()}
                    />
                </Wrapper>
            </Background>
        );
    }
}

export default WithFeedBack(TestScreen);
