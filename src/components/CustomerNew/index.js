import React, { Component } from 'react';
import axios from 'axios';
import { TouchableHighlight } from 'react-native';
import cepPromise from 'cep-promise';
import { TextField, TextWrapper, ScrollWrapper } from '@Styles';
import Button from '../Button';
import Background from '../Background';
import WithFeedBack from '../WithFeedback';
import string from '@Strings';
import { SearchImage } from '../../styles';

const Searchimg = require('@Image/search.png');
class CustomerNew extends Component {
    state = {
        cpf_cnpj: '',
        nome: '',
        telefone: '',
        celular: '',
        email: '',
        cep: '',
        rua: '',
        numero:'',
        bairro: '',
        cidade: '',
        uf: '',
        complemento: '',
        state: '',
        city: '',
        neighborhood: '',
        street: '',
        responseCep: [],
    };

    changeState = (field, value) => {
        this.setState({ [field]: value });
    };


    buscaCep = () => {
        const {
          cep,

        } = this.state;
        const { setLoading, showToast } = this.props;

        const valCep = /^[0-9]{2}[0-9]{3}[0-9]{3}$/;

        if (valCep) {
            setLoading(true);
            console.log(this.state.cep)
            cepPromise(`${this.state.cep}`)
            .then(response => {
                this.setState({
                    responseCep: response,
                    state: response.state,
                    city: response.city,
                    neighborhood: response.neighborhood,
                    street: response.street
                })
            })
            .then(() => setLoading(false))
            .catch((error) => {
                showToast({ message: 'Cep inválido tente novamente', type: `error` });
                
                console.log(error)
            })
            .then(() => setLoading(false))
        } else {
            console.log(this.state.cep)
         
            
        }
    };

    registerNewCustomer = () => {
        const {
            cpf_cnpj,
            nome,
            telefone,
            celular,
            email,
            cep,
            rua,
            numero,
            bairro,
            cidade,
            uf,
            complemento,
        } = this.state;

        const { setLoading, showToast } = this.props;

        if (nome && cpf_cnpj) {
            setLoading(true);
            axios
                .post('https://agile-hollows-47291.herokuapp.com/api/v1/cliente', {
                    cpf_cnpj: this.state.cpf_cnpj,
                    nome: this.state.nome,
                    telefone: this.state.telefone,
                    celular: this.state.celular,
                    email: this.state.email,
                    cep: this.state.cep,
                    rua: this.state.street,
                    numero: this.state.numero,
                    bairro: this.state.neighborhood,
                    cidade: this.state.city,
                    uf: this.state.state,
                    complemento: this.state.complemento,
                    cadastrante_id: 1,
                })
                .then(response => {
                    this.setState({
                        cpf_cnpj: '',
                        nome: '',
                        telefone: '',
                        celular: '',
                        email: '',
                        cep: '',
                        rua: '',
                        numero:'',
                        bairro: '',
                        cidade: '',
                        uf: '',
                        complemento: '',
                        state: '',
                        city: '',
                        neighborhood: '',
                        street: ''
                    });
                    showToast({
                        message: string.Customer.registerNewCustomerSuccess,
                        type: `success`,
                    });
                    //deviceStorage.saveItem ('access_token', data.access_token);
                })
                .catch()
                .then(() => setLoading(false));
        } else {
            showToast({ message: string.Login.insertFields, type: `error` });
        }
    };

    render() {
        const { navigation } = this.props;
        return (
            <Background>
                <ScrollWrapper>
                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.cnpj}
                            value={this.state.cpf_cnpj}
                            keyboardType={'numeric'}
                            onChangeText={text => this.changeState('cpf_cnpj', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.customerName}
                            value={this.state.nome}
                            onChangeText={text => this.changeState('nome', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.telephone}
                            value={this.state.telefone}
                            keyboardType={'numeric'}
                            onChangeText={text => this.changeState('telefone', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.cellphone}
                            value={this.state.celular}
                            keyboardType={'numeric'}
                            onChangeText={text => this.changeState('celular', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.email}
                            value={this.state.email}
                            onChangeText={text => this.changeState('email', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.zipCode}
                            value={this.state.cep}
                            keyboardType={'numeric'}
                            onChangeText={text => this.changeState('cep', text)}
                        />
                        <TouchableHighlight underlayColor = {'white'} onPress={() => this.buscaCep()}>
                            <SearchImage source={Searchimg}  />
                        </TouchableHighlight>  
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.adress}
                            value={this.state.street}
                            onChangeText={text => this.changeState('rua', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.number}
                            value={this.state.numero}
                            keyboardType={'numeric'}
                            onChangeText={text => this.changeState('numero', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.neighborhood}
                            value={this.state.neighborhood}
                            onChangeText={text => this.changeState('bairro', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.city}
                            value={this.state.city}
                            onChangeText={text => this.changeState('cidade', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.state}
                            value={this.state.state}
                            onChangeText={text => this.changeState('uf', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.adressLineTwo}
                            value={this.state.complemento}
                            onChangeText={text => this.changeState('complemento', text)}
                        />
                    </TextWrapper>

                    <Button
                        primary
                        title={string.Customer.save}
                        onPress={() => this.registerNewCustomer(navigation.navigate('CustomerList'))}
                    />
                </ScrollWrapper>
            </Background>
        );
    }
}
export default WithFeedBack(CustomerNew);
