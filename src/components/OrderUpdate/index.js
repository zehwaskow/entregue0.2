import React, { Component } from 'react';
import axios from 'axios';
import {
    ScrollWrapper,
    OrderWrapper,
    ProductDetails,
    ListItem,
    Pick,
    Label,
    ProductImage,
    Card,
    Wrapper,
    TextWrapper,
    TextField,
    ItemWrapper,
    TextShowTitle,
    TextShowItem,
} from '@Styles';
import { FlatList, Text } from 'react-native';
import Background from '../Background';
import Button from '../Button';
import WithFeedBack from '../WithFeedback';
import string from '@Strings';

const noImage = require('@Image/no_image.jpg');

class OrderUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pedidoJoinItem: [],
            pedidoJoinCliente: [],
            pedido: '',
            total: '',
            pedidoSumItem: [],
            forma_pagamento: '',
            status: '',
            statusInicial: '',
            formaInicial: '',
            total:'',
            status:'',
            criado_em:'',
            cliente_id:'',
            cadastrante_id:'',
            forma_pagamento:'',
            data_prevista:'',
            data_entrega:'',
            imagem: '',
        };
    }

    updateOrder = () => {
        const {
          total,
          status,
          criado_em,
          cliente_id,
          cadastrante_id,
          forma_pagamento,
          data_prevista,
          data_entrega,
        } = this.state;
        const { setLoading, showToast } = this.props;
    
        if (this.state.status) {
          setLoading(true);
          axios
            .put(`https://agile-hollows-47291.herokuapp.com/api/v1/pedido/${this.state.pedido_id}`, {
              total: '0.00',
              status: this.state.status,
              criado_em: '2018-11-22 19:10:25',
              cliente_id: this.state.cliente_id,
              cadastrante_id: 1,
              forma_pagamento: this.state.forma_pagamento,
              data_prevista: '2018-12-03',
              data_entrega: '2018-11-22 19:10:25',
            })
            .then(response => {
              this.setState({
                total: '',
                status: '',
                criado_em: '',
                cliente_id: '',
                cadastrante_id: '',
                forma_pagamento: '',
                data_prevista: '',
                data_entrega: '',
              });
              showToast({
                message: 'Pedido Criado com Sucesso!',
                type: `success`,
              });
            })
            .catch()
            .then(() => setLoading(false));
        } else {
          showToast({ message: 'Erro ao Criar Pedido', type: `error` });
        }
      };

    componentWillMount() {
        const { params } = this.props.navigation.state;
        axios
            .get(
                'https://agile-hollows-47291.herokuapp.com/api/v1/pedidoSumItem/' +
                params.id
            )
            .then(response => {
                this.setState({
                    pedidoSumItem: response.data.data[0].result,
                });
            })
            .catch(error => {
   
                console.log(
                    'An error occured while we trying to recovering the data MAICAO 3'
                );
            });
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;
        axios
            .get(
                'https://agile-hollows-47291.herokuapp.com/api/v1/pedidoJoinCliente/' +
                params.id
            )
            .then(response => {
                this.setState({
                    pedidoJoinCliente: response.data.data,
                    formaInicial: response.data.data[0].forma_pagamento,
                    statusInicial: response.data.data[0].status,
                    cliente_id: response.data.data[0].cliente_id,
                    pedido_id: response.data.data[0].id,

                });
            })
            .catch(error => {
                
                console.log(
                    'An error occured while we trying to recovering the data MAICAO 1'
                );
            });
        axios
            .get(
                'https://agile-hollows-47291.herokuapp.com/api/v1/pedidoJoinItem/' +
                params.id
            )
            .then(response => {
                this.setState({
                    pedidoJoinItem: response.data.data,
                });
                console.log(this.state.pedidoJoinItem)
            })
            .catch(error => {
              
                console.log(
                    'An error occured while we trying to recovering the data MAICAO 2'
                );
            });
    }

    renderCustomer() {
        return this.state.pedidoJoinCliente.map(cliente => {
            return (
                <Card>
                    <OrderWrapper>
                        <Label>
                            {string.Customer.customerName}
                            {cliente.nome}
                        </Label>
                        <TextShowItem>
                            {string.Order.status}
                            {cliente.status}
                        </TextShowItem>
                        <TextShowItem>
                            {string.Order.payment}
                            {cliente.forma_pagamento}
                        </TextShowItem>
                        <TextShowItem>
                            {string.Customer.cnpj}
                            {cliente.cep}
                        </TextShowItem>
                        <TextShowItem>
                            {string.Customer.street}
                            {cliente.rua}
                        </TextShowItem>
                        <TextShowItem>
                            {string.Customer.number}
                            {cliente.numero}
                        </TextShowItem>
                        <TextShowItem>
                            {string.Customer.neighborhood}
                            {cliente.bairro}
                        </TextShowItem>
                        <TextShowItem>
                            {string.Customer.city}
                            {cliente.cidade}
                        </TextShowItem>
                        <TextShowItem>
                            {string.Customer.state}
                            {cliente.uf}
                        </TextShowItem>
                        <TextShowItem>
                            {string.Customer.adressLineTwo}
                            {cliente.complemento}
                        </TextShowItem>
                    </OrderWrapper>
                </Card>
            );
        });
    }

    renderItem = ({ item }) => {
        const { navigation } = this.props;
        return (
            <Card>

                <ListItem>
                    <ProductImage source={{ uri: `${item.imagem}` } || noImage} />

                    <ProductDetails>
                        <Text>
                        {string.Order.product}
                        {item.nome}
                        </Text>

                        <Text>
                        {string.Order.brand}
                        {item.marca}
                        </Text>

                        <Text>
                        {string.Order.amount}
                        {item.quantidade}
                        </Text>
                        <Text>
                        {string.Order.unitaryPrice}
                        {item.valor_unitario}
                        </Text>
                    </ProductDetails>

                </ListItem>   
              
            </Card>

        );
    };

    render() {
        const { navigation } = this.props;
        return (
            <Background>
                <ScrollWrapper>
                    <Card>
                        <Wrapper>
                            <Label>Itens do Pedido: </Label>
                        </Wrapper>
                        <TextWrapper>
                            <FlatList
                                data={this.state.pedidoJoinItem}
                                renderItem={this.renderItem}
                                keyExtractor={(x, i) => i}
                            />
                        </TextWrapper>
                        <Card>
                            <TextWrapper>
                                <TextShowItem>
                                    <Label>
                                        Total: {this.state.pedidoSumItem}
                                    </Label>
                                </TextShowItem>
                            </TextWrapper>
                        </Card>
                    </Card>
                    <Card>
                        <Wrapper>
                            <Label>Dados do Cliente: </Label>
                        </Wrapper>
                        {this.renderCustomer()}
                    </Card>
                    <Card>
                        <Wrapper>
                            <Wrapper>
                                <Label>Alterar Pedido: </Label>
                            </Wrapper>
                        </Wrapper>
                        <Card>
                            <TextWrapper>
                                <Label>Forma de Pagamento: </Label>
                            </TextWrapper>
                            <TextWrapper>
                                <Pick
                                    selectedValue={this.state.forma_pagamento}
                                    onValueChange={(itemValue, itemIndex) =>
                                        this.setState({ forma_pagamento: itemValue })}
                                >
                                    <Pick.Item label={this.state.formaInicial} value={this.state.formaInicial} />
                                    <Pick.Item label="Dinheiro" value="Dinheiro" />
                                    <Pick.Item label="Cartão de Débito" value="Cartão de Débito" />
                                    <Pick.Item
                                        label="Cartão de Crédito"
                                        value="Cartão de Crédito"
                                    />
                                    <Pick.Item label="Boleto Bancário" value="Boleto Bancário" />
                                </Pick>
                            </TextWrapper>
                        </Card>

                        <Card>
                            <TextWrapper>
                                <Label>Status: </Label>
                            </TextWrapper>
                            <TextWrapper>
                                <Pick
                                    selectedValue={this.state.status}
                                    onValueChange={(itemValue, itemIndex) =>
                                        this.setState({ status: itemValue })}
                                >
                                    <Pick.Item label={this.state.statusInicial} value={this.state.statusInicial} />
                                    <Pick.Item
                                        label="Aguardando Entrega"
                                        value="Aguardando Entrega"
                                    />
                                    <Pick.Item
                                        label="Aguardando Estoque"
                                        value="Aguardando Estoque"
                                    />
                                    <Pick.Item
                                        label="Saiu Para Entrega"
                                        value="Saiu Para Entrega"
                                    />
                                    <Pick.Item label="Entregue" value="Entregue" />
                                </Pick>
                            </TextWrapper>
                        </Card>
                        <Button
                            primary
                            title={string.Products.save}
                            onPress={() => this.updateOrder(navigation.navigate('OrderList'))}
                        />
                    </Card>
                </ScrollWrapper>
            </Background>
        );
    }
}

export default WithFeedBack(OrderUpdate);
