import React, { Component } from "react";
import { TextField, TextWrapper, EndWrapper } from "@Styles";
import Button from "../Button";
import string from "@Strings";
import Background from "../Background";
import WithFeedBack from '../WithFeedback';
import styled from "styled-components";

class SignUp extends Component {
    state = {
        nome: '',
        email: '',
        senha: '',
        confirmarSenha: '',
    };

    changeState = (field, value) => {
        this.setState({ [field]: value });
    };

    signUp = () => {
        const {
            nome,
            email,
            senha,
            confirmarSenha,
        } = this.state;
        const { setLoading, showToast } = this.props;

        if (email && password && confirmarSenha && password === confirmarSenha) {
            setLoading(true);
            axios
                .post('https://agile-hollows-47291.herokuapp.com/api/v1/usuario', {
                    nome: this.state.nome,
                    email: this.state.email,
                    senha: this.state.senha,
                })
                .then(response => {
                    this.setState({
                        nome: '',
                        email: '',
                        senha: '',
                        confirmarSenha: '',
                    });
                    showToast({
                        message: string.Products.registerNewUserSuccess,
                        type: `success`,
                    });
                    //deviceStorage.saveItem ('access_token', data.access_token);
                })
                .catch()
                .then(() => setLoading(false));
        } else {
            showToast({ message: string.User.insertFieldsUSer, type: `error` });
        }
    }

    render() {
        const {
            nome,
            email,
            senha,
            confirmarSenha
        } = this.state;
        const { navigation } = this.props;
        return (
            <Background>
                <TextWrapper>
                    <TextField
                        placeholder={string.Login.name}
                        onChangeText={text => this.changeState("nome", text)}
                        value={nome}
                    />
                </TextWrapper>
                <TextWrapper>
                    <TextField
                        placeholder={string.Login.email}
                        onChangeText={text => this.changeState("email", text)}
                        value={email}
                        keyboardType={"email-address"}
                        autoCapitalize={'none'}
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Login.password}
                        onChangeText={text => this.changeState("senha", text)}
                        value={senha}
                        secureTextEntry
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Login.confirmPassword}
                        onChangeText={text => this.changeState("confirmarSenha", text)}
                        value={confirmarSenha}
                        secureTextEntry
                    />
                </TextWrapper>

                <EndWrapper>
                    <Button
                        primary
                        onPress={this.signUp()}
                        title={string.Login.register}
                    />
                </EndWrapper>
            </Background>
        );
    }
}

export default WithFeedBack(SignUp);
