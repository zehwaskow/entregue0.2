import React, { Component } from 'react';
import axios from 'axios';
import { TextWrapper, TextField, Pick, Wrapper } from '@Styles';
import Button from '../Button';
import Background from '../Background';
import WithFeedBack from '../WithFeedback';
import string from '@Strings';

class ProductUpdate extends Component {
    state = {
        id: '',
        nome: '',
        marca: '',
        estoque: '',
        preco: '',
        categoria: '',
    };

    changeState = (field, value) => {
        this.setState({ [field]: value });
    };

    updateProduct = () => {
        const {
            id,
            nome,
            marca,
            estoque,
            preco,
            categoria,
        } = this.state;
        const { setLoading, showToast } = this.props;

        if (nome && marca) {
            setLoading(true);
            axios
                .put(`https://agile-hollows-47291.herokuapp.com/api/v1/produto/${this.state.id}`, {
                    nome: this.state.nome,
                    marca: this.state.marca,
                    estoque: this.state.estoque,
                    preco: this.state.preco,
                    categoria: this.state.categoria,
                    proprietario_id: 1,
                })
                .then(response => {
                    this.setState({
                        nome: '',
                        marca: '',
                        estoque: '',
                        preco: '',
                        categoria: '',
                    });
                    showToast({
                        message: string.Products.registerNewProductSuccess,
                        type: `success`,
                    });
                    //deviceStorage.saveItem ('access_token', data.access_token);
                })
                .catch()
                .then(() => setLoading(false));
        } else {
            showToast({ message: string.Login.insertFields, type: `error` });
        }
    };


    componentDidMount() {
        const { params } = this.props.navigation.state;
        axios.get
            ('https://agile-hollows-47291.herokuapp.com/api/v1/produto/' + params.id)
            .then(response => {
                this.setState({
                    dataSource: response.data.data,
                    id: response.data.data.id,
                    nome: response.data.data.nome,
                    marca: response.data.data.marca,
                    estoque: response.data.data.estoque,
                    preco: response.data.data.preco,
                    categoria: response.data.data.categoria,
                })
            })

            .catch((error) => {
                console.error(error)
                console.log('An error occured while we trying to recovering the data')
            });
    }

    render() {
        const {
            nome,
            marca,
            estoque,
            preco,
            categoria,
        } = this.state;
        console.log('ESTOQUE', this.state.estoque)
        const { navigation } = this.props;
        const { params } = this.props.navigation.state;

        return (
            <Background>
                <TextWrapper>
                    <TextField
                        placeholder={string.Products.name}
                        value={nome}
                        onChangeText={text => this.changeState('nome', text)}
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Products.brand}
                        value={marca}
                        onChangeText={text => this.changeState('marca', text)}
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Products.stock}
                        value={this.state.estoque.toString()}
                        keyboardType={'numeric'}
                        onChangeText={text => this.changeState('estoque', text)}
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Products.price}
                        value={preco}
                        keyboardType={'numeric'}
                        onChangeText={text => this.changeState('preco', text)}
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Products.price}
                        value={categoria}
                        onChangeText={text => this.changeState('categoria', text)}
                    />
                </TextWrapper>

                <Wrapper>
                    <Button
                        title={string.Products.image}
                        onPress={() => navigation.navigate('Login')}
                    />
                    <Button
                        primary
                        title={string.Products.save}
                        onPress={() => this.updateProduct(navigation.navigate('ProductsList'))}
                    />
                </Wrapper>
            </Background>
        );
    }
}

export default WithFeedBack(ProductUpdate);
