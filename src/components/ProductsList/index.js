import React, { Component } from 'react';
import axios from 'axios';
import {
    ScrollWrapper,
    Wrapper,
    LoadContainer,
    RNButton,
    ListSeparator,
    ProductImage,
    ListItem,
    ProductDetails,
    TextWrapper,
    Card,
    TextField,
    ItemWrapper,
    TextShowTitle,
    TextShowItem,
} from '@Styles';
import { FlatList, Text, TouchableHighlight, ActivityIndicator } from 'react-native';
import Background from '../Background';
import Button from '../Button';
import string from '@Strings';
import WithFeedBack from '../WithFeedback';
import { SearchImage } from '../../styles';

const noImage = require('@Image/no_image.jpg');
const Searchimg = require('@Image/search.png');

class ProductsList extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <RNButton
                    primary
                    onPress={() => navigation.navigate(`ProductNew`)}
                    title="Novo"
                />
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            busca: '',
            loading: true,
        };
    }

    changeState = (field, value) => {
        this.setState({ [field]: value });
    };

    componentWillUpdate() {
        this.state.loading = true
        axios
            .get('https://agile-hollows-47291.herokuapp.com/api/v1/produto')
            .then(response => {
                this.setState({ dataSource: response.data.data });
            })

            .catch(error => {
                console.error(error);
                console.log('An error occured while we trying to recovering the data');
            });
            this.state.loading = false

    }

    componentDidMount() {
        this.state.loading = true
        axios
            .get('https://agile-hollows-47291.herokuapp.com/api/v1/produto')
            .then(response => {
                this.setState({ dataSource: response.data.data });
            })
            .catch(error => {
                console.error(error);
                console.log('An error occured while we trying to recovering the data');
            });
            this.state.loading = false
    }

    searchProduct = () => {
        const { busca } = this.state;

        if(busca){
            this.state.loading = true;
        axios
            .get(
                `https://agile-hollows-47291.herokuapp.com/api/v1/produto/?nome=${this.state.busca}`,{
            })
            .then(response => {
                this.setState({busca: ''});
                console.log(response);
            })
            .catch(error => {
                console.error(error);
                console.log('An error occured while we trying to recovering the data');
            });
            this.state.loading = false

        }
    }


    renderItem = ({ item }) => {
        const { navigation } = this.props;
        return (
            <TouchableHighlight
            underlayColor = {'white'} onPress={() => navigation.navigate('ProductUpdate', { id: item.id })}
            >
                <ListItem>
                    <ProductImage source={{ uri: `${item.imagem}` } || noImage} />

                    <ProductDetails>
                        <Text>
                            {item.marca}
                        </Text>

                        <Text>
                            {item.nome}
                        </Text>

                        <Text>
                            {item.preco}
                        </Text>
                    </ProductDetails>

                </ListItem>
            </TouchableHighlight>
        );
    };

    render() {
        if (this.state.loading === true) {
            return (
                <Background>
                    <LoadContainer>
                        <ActivityIndicator size={80} />
                    </LoadContainer>
                </Background>

            );
        }
        return (
            <Background>
                <ScrollWrapper>
                    <TextWrapper>
                        <TextField
                            placeholder={string.Products.search}
                            onChangeText={text => this.changeState('busca', text)}
                        />
                        <TouchableHighlight underlayColor = {'white'} onPress={() => this.searchProduct()}>
                            <SearchImage source={Searchimg} />
                        </TouchableHighlight>
                    </TextWrapper>

                    <TextWrapper>
                        <FlatList
                            data={this.state.dataSource}
                            renderItem={this.renderItem}
                            ItemSeparatorComponent={() => <ListSeparator />}
                            keyExtractor={(x, i) => i}
                        />
                    </TextWrapper>
                </ScrollWrapper>
            </Background>
        );
    }
}

export default ProductsList;
