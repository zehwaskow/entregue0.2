import React, { Component } from 'react';
import axios from 'axios';
import {
    ScrollWrapper,
    Wrapper,
    RNButton,
    Label,
    ListSeparator,
    ProductImage,
    LoadContainer,
    ListItem,
    ProductDetails,
    TextWrapper,
    Card, TextField,
    ItemWrapper,
    TextShowTitle,
    TextShowItem
} from "@Styles";
import { FlatList, TouchableHighlight, Text, ActivityIndicator } from 'react-native';
import Background from "../Background";
import Button from '../Button';
import WithFeedBack from '../WithFeedback';
import string from "@Strings";
import { SearchImage } from '../../styles';

const customerImage = require('@Image/Customer-512.png');
const Searchimg = require('@Image/search.png');

class CustomerList extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <RNButton
                    onPress={() => navigation.navigate(`CustomerNew`)}
                    title="Novo"
                />
            ),
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            cliente: '',
            busca: '',
            loading: true,
        };
    }

    changeState = (field, value) => {
        this.setState({ [field]: value });
    };


    componentWillUpdate() {
        this.state.loading = true;
        axios.get('https://agile-hollows-47291.herokuapp.com/api/v1/cliente')
        .then(response => {
            console.log(response.data.data)
            this.setState({ dataSource: response.data.data })
        })

        .catch((error) => {
            console.error(error)
            console.log('An error occured while we trying to recovering the data')
        })
        this.state.loading = false;
    }

    componentDidMount() {
        this.state.loading = true;
        axios.get('https://agile-hollows-47291.herokuapp.com/api/v1/cliente')
            .then(response => {
                console.log(response.data.data)
                this.setState({ dataSource: response.data.data })
            })

            .catch((error) => {
                console.error(error)
                console.log('An error occured while we trying to recovering the data')
            })
            this.state.loading = false;
    }

    searchCustomer = () => {
        const {
            busca,
            loading
           
        } = this.state;
        this.state.loading = true;        
            axios
                .get(`https://agile-hollows-47291.herokuapp.com/api/v1/cliente/?nome=${this.state.busca}`, {
                 
                })
                .then(response => {
                    this.setState({
                        busca: ''
                    });
                    console.log(response)
                    //deviceStorage.saveItem ('access_token', data.access_token);
                })
                .catch()
                this.state.loading = false;
            }




    renderItem = ({ item }) => {
        const { navigation } = this.props;
        return (
            <TouchableHighlight underlayColor = {'white'} onPress={() => navigation.navigate('CustomerUpdate', { id: item.id })}>
                <ListItem>
                    <ProductImage source={customerImage} />

                    <ProductDetails>
                        <Label>
                            {string.Customer.customerName}
                            {item.nome}
                        </Label>

                        <Text>
                            {string.Customer.cnpj}
                            {item.cpf_cnpj}
                        </Text>

                        <Text>
                            {string.Customer.email}
                            {item.email}
                        </Text>
                        <Text>
                            {string.Customer.cellphone}
                            {item.celular}
                        </Text>
                    </ProductDetails>

                </ListItem>
            </TouchableHighlight>
        );
    }


    render() {
        if (this.state.loading === true) {
            return (
                <Background>
                    <LoadContainer>
                        <ActivityIndicator size={80} />
                    </LoadContainer>
                </Background>

            );
        }
        return (
            <Background>
                <ScrollWrapper>
                    <TextWrapper>
                  
                        <TextField
                            placeholder={string.Customer.search}
                            value={this.state.busca}
                            onChangeText={text => this.changeState('busca', text)}
                        />
                          <TouchableHighlight onPress={() => this.searchCustomer()}>
                            <SearchImage source={Searchimg}  />
                        </TouchableHighlight>  
                    </TextWrapper>
                    <TextWrapper>
                        <FlatList
                            data={this.state.dataSource}
                            renderItem={this.renderItem}
                            ItemSeparatorComponent={() => <ListSeparator />}
                            keyExtractor={(x, i) => i}
                        />
                    </TextWrapper>

                </ScrollWrapper>
            </Background>
        );
    }
}

export default CustomerList;