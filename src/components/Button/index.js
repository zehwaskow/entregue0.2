import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Colors from '@Styles/colors';

export const Container = styled.TouchableHighlight`  
  background-color: ${props => (props.primary ? Colors.primaryButton : Colors.textButton)};
  border-color: ${props => (props.primary ? Colors.textButton : Colors.primaryButton)};
  border-width: 1px;
  padding-top: 14px;
  padding-right: 14px;
  padding-left: 14px;
  padding-bottom: 14px;
  width: 100%;
  justify-content: center;
  align-content: center;
  border-radius: 5px;
  margin-top: 10px;
  margin-bottom: 10px;  
`;

const Text = styled.Text`
  text-align: center;
  color: ${props => (props.primary ? Colors.textButton : Colors.primaryButton)};
`;

const Button = ({primary, title, onPress}) => (
  <Container primary={primary} onPress={onPress}>
    <Text primary={primary}>{title}</Text>
  </Container>
);

Button.propTypes = {
  title: PropTypes.string.isRequired,
  primary: PropTypes.bool,
};

export default Button;
