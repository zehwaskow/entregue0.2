import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Img = require ('@Image/back.png');

const Container = styled.ImageBackground`
  flex: 1;
  padding: ${({noPadding}) => (!noPadding ? `14px` : `0`)};  
`;

const Background = ({children, noPadding}) => (
  <Container
    source={Img}
    imageStyle={{resizeMode: 'stretch'}}
    noPadding={noPadding}
  >
    {children}
  </Container>
);

Background.propTypes = {
  children: PropTypes.arrayOf (PropTypes.any).isRequired,
};

export default Background;
