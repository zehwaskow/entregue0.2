import React, { Component } from 'react';
import { Text, AsyncStorage } from 'react-native';
import axios from 'axios';
import { TextField, TextWrapper, ScrollWrapper, ForgotButton, Logo, Wrapper } from '@Styles';
import Button from '../Button';
import Background from '../Background';
import string from '@Strings';
import WithFeedBack from '../WithFeedback';
import deviceStorage from '../Services/deviceStorage';

const logo = require('@Image/entregue.png');

class Login extends Component {
  state = {
    username: '',
    password: '',
  };

  changeState = (field, value) => {
    this.setState({ [field]: value });
  };

  authenticate = () => {
    const { username, password } = this.state;
    const { setLoading, showToast, navigation } = this.props;



    if (username && password) {
      navigation.navigate('App');

      /*setLoading (true);
      axios
        .post ('http://192.168.0.101:8080/oauth/token', {
          username,
          password,
        })
        .then (({data}) => {
          deviceStorage.saveItem ('access_token', data.access_token);
        })
        .catch (err => console.log ('OIA A COCADA', err))
        .then (() => setLoading (false)); */
    } else {
      showToast({ message: string.Login.insertFields, type: `error` });
    }
  };


  redirect = async token => {
    const { navigation } = this.props;

    try {
      await AsyncStorage.setItem('token', token);
      navigation.navigate('App');
    } catch (error) {
      console.log(error);
    }
  };


  componentDidMount() {
    axios.get('https://agile-hollows-47291.herokuapp.com/api/v1/cliente')
      .then(response => {
        console.log('OPEN')
      })
      .catch((error) => {
        console.log('An error occured while we trying to recovering the data')
      });
  }

  render() {
    const { username, password } = this.state;
    const { navigation } = this.props;

    return (
      <Background>
        <ScrollWrapper>
          <Wrapper>
            <Logo source={logo} />
          </Wrapper>

          <Wrapper>
            <TextWrapper>
              <TextField
                placeholder={string.Login.login}
                onChangeText={text => this.changeState('username', text)}
                value={username}
                keyboardType="email-address"
                autoCapitalize="none"
              />
            </TextWrapper>

            <TextWrapper>
              <TextField
                placeholder={string.Login.password}
                onChangeText={text => this.changeState('password', text)}
                value={password}
                secureTextEntry
                returnKeyType="send"
              />
            </TextWrapper>

            <ForgotButton onPress={() => navigation.navigate('ForgotPassword')}>
              <Text>{string.Login.forgotPassword}</Text>
            </ForgotButton>
          </Wrapper>

          <Wrapper>
            <Button
              primary
              title={string.Login.login}
              onPress={() => this.authenticate()}
            />
            <Button
              title={string.Login.register}
              onPress={() => navigation.navigate('SignUp')}
            />
          </Wrapper>
        </ScrollWrapper>
      </Background>
    );
  }
}

export default WithFeedBack(Login);
