import React, { Component } from 'react';
import axios from 'axios';
import { TextField, TextWrapper, ScrollWrapper } from '@Styles';
import Button from '../Button';
import Background from '../Background';
import WithFeedBack from '../WithFeedback';
import string from '@Strings';

class CustomerUpdate extends Component {
    state = {
        id: '',
        cpf_cnpj: '',
        nome: '',
        telefone: '',
        telefone: '',
        celular: '',
        email: '',
        cep: '',
        rua: '',
        bairro: '',
        cidade: '',
        uf: '',
        numero: '',
        complemento: '',
    };

    changeState = (field, value) => {
        this.setState({ [field]: value });
    };

    updateCustomer = () => {
        const {
            id,
            cpf_cnpj,
            nome,
            telefone,
            celular,
            email,
            cep,
            rua,
            bairro,
            cidade,
            uf,
            complemento,
        } = this.state;
        const { setLoading, showToast } = this.props;
        if (nome && cpf_cnpj) {
            setLoading(true);
            axios
                .put(`https://agile-hollows-47291.herokuapp.com/api/v1/cliente/${this.state.id}`, {
                    cpf_cnpj: this.state.cpf_cnpj,
                    nome: this.state.nome,
                    telefone: this.state.telefone,
                    telefone: this.state.telefone,
                    celular: this.state.celular,
                    email: this.state.email,
                    cep: this.state.cep,
                    rua: this.state.rua,
                    bairro: this.state.bairro,
                    cidade: this.state.cidade,
                    uf: this.state.uf,
                    complemento: this.state.complemento,
                    cadastrante_id: 1,
                })
                .then(response => {
                    this.setState({
                        id: '',
                        cpf_cnpj: '',
                        nome: '',
                        telefone: '',
                        telefone: '',
                        celular: '',
                        email: '',
                        cep: '',
                        rua: '',
                        bairro: '',
                        cidade: '',
                        numero:'',
                        uf: '',
                        complemento: '',
                    });
                    showToast({
                        message: string.Customer.registerNewCustomerSuccess,
                        type: `success`,
                    });
                    //deviceStorage.saveItem ('access_token', data.access_token);
                })
                .catch()
                .then(() => setLoading(false));
        } else {
            showToast({ message: string.Login.insertFieldsCustomer, type: `error` });
        }
    };

    componentDidMount() {
        const { params } = this.props.navigation.state;
        axios.get
            ('https://agile-hollows-47291.herokuapp.com/api/v1/cliente/' + params.id)
            .then(response => {
                console.log(response.data.data)
                this.setState({
                    dataSource: response.data.data,
                    id: response.data.data.id,
                    cpf_cnpj: response.data.data.cpf_cnpj,
                    nome: response.data.data.nome,
                    telefone: response.data.data.telefone,
                    celular: response.data.data.celular,
                    email: response.data.data.email,
                    cep: response.data.data.cep,
                    numero: response.data.data.numero,
                    rua: response.data.data.rua,
                    bairro: response.data.data.bairro,
                    cidade: response.data.data.cidade,
                    uf: response.data.data.uf,
                    complemento: response.data.data.complemento,
                })
            })

            .catch((error) => {
                console.error(error)
                console.log('An error occured while we trying to recovering the data')
            });
    }


    render() {
        const { params } = this.props.navigation.state;
        const { navigation } = this.props;
        return (
            <Background>
                <ScrollWrapper>
                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.cnpj}
                            value={this.state.cpf_cnpj}
                            keyboardType={'numeric'}
                            onChangeText={text => this.changeState('cpf_cnpj', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.customerName}
                            value={this.state.nome}
                            onChangeText={text => this.changeState('nome', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.telephone}
                            value={this.state.telefone}
                            keyboardType={'numeric'}
                            onChangeText={text => this.changeState('telefone', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.cellphone}
                            value={this.state.celular}
                            keyboardType={'numeric'}
                            onChangeText={text => this.changeState('celular', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.email}
                            value={this.state.email}
                            onChangeText={text => this.changeState('email', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.zipCode}
                            value={this.state.cep.toString()}
                            keyboardType={'numeric'}
                            onChangeText={text => this.changeState('cep', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.adress}
                            value={this.state.rua}
                            onChangeText={text => this.changeState('rua', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.number}
                            value={this.state.numero.toString()}
                            onChangeText={text => this.changeState('numero', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.neighborhood}
                            value={this.state.bairro}
                            onChangeText={text => this.changeState('bairro', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.city}
                            value={this.state.cidade}
                            onChangeText={text => this.changeState('cidade', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.state}
                            value={this.state.uf}
                            onChangeText={text => this.changeState('uf', text)}
                        />
                    </TextWrapper>

                    <TextWrapper>
                        <TextField
                            placeholder={string.Customer.adressLineTwo}
                            value={this.state.complemento}
                            onChangeText={text => this.changeState('complemento', text)}
                        />
                    </TextWrapper>

                    <Button
                        primary
                        title={string.Customer.save}
                        onPress={() => this.updateCustomer(navigation.navigate('CustomerList'))}
                    />
                </ScrollWrapper>
            </Background>
        );
    }
}
export default WithFeedBack(CustomerUpdate);
