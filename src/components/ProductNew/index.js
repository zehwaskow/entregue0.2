import React, { Component } from 'react';
import axios from 'axios';
import { TextWrapper, TextField, Pick, Wrapper } from '@Styles';
import Button from '../Button';
import Background from '../Background';
import WithFeedBack from '../WithFeedback';
import string from '@Strings';

class ProductNew extends Component {
    state = {
        nome: '',
        marca: '',
        estoque: '',
        preco: '',
        categoria: '',
        imagem: '',
    };

    changeState = (field, value) => {
        this.setState({ [field]: value });
    };

    registerNewProduct = () => {
        const {
            nome,
            marca,
            estoque,
            preco,
            categoria,
            imagem,
        } = this.state;
        const { setLoading, showToast } = this.props;

        if (nome && marca) {
            setLoading(true);
            axios
                .post('https://agile-hollows-47291.herokuapp.com/api/v1/produto', {
                    nome: this.state.nome,
                    marca: this.state.marca,
                    estoque: this.state.estoque,
                    preco: this.state.preco,
                    categoria: this.state.categoria,
                    imagem: this.state.imagem,
                    proprietario_id: 1,
                })
                .then(response => {
                    this.setState({
                        nome: '',
                        marca: '',
                        estoque: '',
                        preco: '',
                        categoria: '',
                        imagem: ''
                    });
                    showToast({
                        message: string.Products.registerNewProductSuccess,
                        type: `success`,
                    });
                    //deviceStorage.saveItem ('access_token', data.access_token);
                })
                .catch()
                .then(() => setLoading(false));
        } else {
            showToast({ message: string.Login.insertFields, type: `error` });
        }
    };

    render() {
        const {
            nome,
            marca,
            estoque,
            preco,
            categoria,
            imagem,
        } = this.state;
        const { navigation } = this.props;
        return (
            <Background>
                <TextWrapper>
                    <TextField
                        placeholder={string.Products.name}
                        value={nome}
                        onChangeText={text => this.changeState('nome', text)}
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Products.brand}
                        value={marca}
                        onChangeText={text => this.changeState('marca', text)}
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Products.stock}
                        value={estoque}
                        keyboardType={'numeric'}
                        onChangeText={text => this.changeState('estoque', text)}
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Products.price}
                        value={preco}
                        keyboardType={'numeric'}
                        onChangeText={text => this.changeState('preco', text)}
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Products.category}
                        value={categoria}
                        onChangeText={text => this.changeState('categoria', text)}
                    />
                </TextWrapper>

                <TextWrapper>
                    <TextField
                        placeholder={string.Products.imagem}
                        value={imagem}
                        onChangeText={text => this.changeState('imagem', text)}
                    />
                </TextWrapper>

                <Wrapper>
                    <Button
                        title={string.Products.image}
                        onPress={() => navigation.navigate('')}
                    />
                    <Button
                        primary
                        title={string.Products.save}
                        onPress={() => this.registerNewProduct(navigation.navigate('ProductsList'))}
                    />
                </Wrapper>
            </Background>
        );
    }
}

export default WithFeedBack(ProductNew);
