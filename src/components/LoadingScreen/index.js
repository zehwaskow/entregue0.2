import React from "react";
import { ActivityIndicator, AsyncStorage } from "react-native";
import Backgrond from "../Background";
import LoadingComponent from "../Loading";

export default class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.getUserToken();
  }

  getUserToken = async () => {
    const userToken = await AsyncStorage.getItem("token");

    this.props.navigation.navigate(userToken ? "App" : "Auth");
  };

  render() {
    return <LoadingComponent />;
  }
}
