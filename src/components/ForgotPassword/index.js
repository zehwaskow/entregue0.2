import React, {Component} from 'react';
import {TextField, TextWrapper, EndWrapper} from '@Styles';
import Button from '../Button';
import Background from '../Background';
import string from '@Strings';

class ForgotPassword extends Component {
  state = {
    email: '',
  };

  changeState = (field, value) => {
    this.setState ({[field]: value});
  };

  sendEmail = () => {
    const {email} = this.state;

    email
      ? resetPassword ({email})
          .then (() => console.log ('deu boa'))
          .catch (err => console.log ('deu ruim', err))
      : console.log ('preenche o email');
  };

  render () {
    const {email} = this.state;
    return (
      <Background>
        <TextWrapper>
          <TextField
            placeholder={string.Login.email}
            onChangeText={text => this.changeState ('email', text)}
            value={email}
            keyboardType={'email-address'}
            autoCapitalize={'none'}
            returnKeyType={'send'}
            onSubmitEditting={this.sendEmail}
          />
        </TextWrapper>

        <EndWrapper>
          <Button primary title={string.Login.send} onPress={this.sendEmail} />
        </EndWrapper>
      </Background>
    );
  }
}

export default ForgotPassword;
