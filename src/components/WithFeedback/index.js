import React from 'react';
import {ActivityIndicator, View, Text} from 'react-native';
import Background from '../Background';
import styled from 'styled-components';

const toastTypes = {
  success: 'green',
  error: 'red',
};

const LoadingContainer = styled.View`
  background-color: transparent;
  position: absolute;
  flex: 1;
  display: flex;
  height: 100%;
  width: 100%;      
  justify-content: center;
  align-items: center;
  z-index: 2;
`;

const Toast = styled.View`
  background-color: ${({color}) => (color ? color : toastTypes.success)}; 
  bottom: 0;
  height: 8%;  
  position: absolute;
  width: 100%;
  justify-content: center;
  padding: 14px;
`;

const ToastMessage = styled.Text`
  color: #FFF;
  font-size: 18;
`;

const renderLoading = () => (
  <LoadingContainer>
    <ActivityIndicator size={80} />
  </LoadingContainer>
);

const renderToast = ({message, type}) => (
  <Toast color={toastTypes[type]}>
    <ToastMessage>{message}</ToastMessage>
  </Toast>
);

export default WrappedComponent => {
  return class WithFeeback extends React.Component {
    state = {
      loading: false,
      toast: false,
      message: ``,
      type: ``,
    };

    setLoading = loading => this.setState ({loading});
    showToast = ({message, type}) => {
      this.setState ({message, toast: true, type});
      setTimeout (() => this.setState ({message: ``, toast: false}), 5000);
    };

    render () {
      const {loading, toast, message, type} = this.state;

      return (
        <Background noPadding>
          {loading && renderLoading ()}
          <WrappedComponent
            {...this.props}
            setLoading={this.setLoading}
            showToast={this.showToast}
          />
          {toast && renderToast ({message, type})}
        </Background>
      );
    }
  };
};
