import React, { Component } from 'react';
import axios from 'axios';
import {
    ScrollWrapper,
    LoadContainer,
    RNButton,
    Wrapper,
    Label,
    ListSeparator,
    ProductImage,
    ListItem,
    ProductDetails,
    TextWrapper,
    Card, TextField,
    ItemWrapper,
    TextShowTitle,
    TextShowItem
} from "@Styles";
import { FlatList, TouchableHighlight, Text, ActivityIndicator } from 'react-native';
import Background from "../Background";
import Button from '../Button';
import WithFeedBack from '../WithFeedback';
import string from "@Strings";
import { SearchImage } from '../../styles';

const cartImage = require('@Image/_ionicons_svg_ios-cart.png');
const Searchimg = require('@Image/search.png');

class OrderList extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerRight: (
                <RNButton
                    onPress={() => navigation.navigate(`OrderNew`)}
                    title="Novo"
                />
            ),
        };
    }

    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            pedido: '',
            idSoma: '',
            busca: '',
            loading: true,
        };
    }

    componentWillUpdate() {
        this.state.loading = true
        axios.get('https://agile-hollows-47291.herokuapp.com/api/v1/pedidoJoinCliente')
            .then(response => {
                this.setState({ dataSource: response.data.data, idSoma: response.data.data.id })
            })
            .catch((error) => {
                console.error(error)
                console.log('An error occured while we trying to recovering the data')
            })
        this.state.loading = false
    }

    componentDidMount() {
        this.state.loading = true
        axios.get('https://agile-hollows-47291.herokuapp.com/api/v1/pedidoJoinCliente')
            .then(response => {
                this.setState({ dataSource: response.data.data, idSoma: response.data.data.id })
            })
            .catch((error) => {
                console.error(error)
                console.log('An error occured while we trying to recovering the data')
            })
        this.state.loading = false
    }

    renderLoading() {

        <LoadingContainer>
            <ActivityIndicator size={80} />
        </LoadingContainer>

    }

    renderItem = ({ item }) => {
        const { navigation } = this.props;
        return (
            <TouchableHighlight underlayColor = {'white'} onPress={() => navigation.navigate('OrderUpdate', { id: item.id })}>
                <ListItem>
                    <ProductImage source={cartImage} />
                    <ProductDetails>
                        <Label>
                            {string.Customer.customerName}
                            {item.nome}
                        </Label>
                        <Text>
                            {string.Customer.cnpj}
                            {item.cpf_cnpj}
                        </Text>
                        <Text>
                            {string.Order.payment}
                            {item.forma_pagamento}
                        </Text>
                        <Text>
                            {string.Order.status}
                            {item.status}
                        </Text>
                    </ProductDetails>
                </ListItem>
            </TouchableHighlight>
        );
    }

    render() {
        if (this.state.loading === true) {
            return (
                <Background>
                    <LoadContainer>
                        <ActivityIndicator size={80} />
                    </LoadContainer>
                </Background>

            );
        }
        return (
            <Background>
                <ScrollWrapper>
                    <TextWrapper>
                        <TextField
                            placeholder={string.Order.search}
                            onChangeText={text => this.changeState('busca', text)}
                        />
                        <TouchableHighlight underlayColor = {'white'} onPress={() => this.searchOrder()}>
                            <SearchImage source={Searchimg} />
                        </TouchableHighlight>
                    </TextWrapper>
                    <TextWrapper>
                        <FlatList
                            data={this.state.dataSource}
                            renderItem={this.renderItem}
                            ItemSeparatorComponent={() => <ListSeparator />}
                            keyExtractor={(x, i) => i}

                        />
                    </TextWrapper>
                </ScrollWrapper>
            </Background>
        );
    }
}

export default OrderList;