import React, { Component } from 'react';
import { View, FlatList, TouchableHighlight, Text } from 'react-native';
import axios from 'axios';
import { TextWrapper, ListSeparator, Card, TextField, Pick, Wrapper, Label, ProductImage, ListItem, ProductDetails } from '@Styles';
import Button from '../Button';
import Background from '../Background';
import string from '@Strings';
import DateTimePicker from 'react-native-modal-datetime-picker';
import TimePicker from 'react-native-modal-datetime-picker';
import WithFeedBack from '../WithFeedback';
import Modal from './modal'
import { ScrollWrapper } from '../../styles';


class OrderNew extends Component {
  state = {
    isDateTimePickerVisible: false,
    modalVisible: false,
    orderId: '',
    response: [],
    products: [],
    selectedProducts: [],
    selectedProduct: ``,
    costumers: [],
    selectedCostumer: ``,
    amount: '',
    discount: '',
    deliveryDate: '',
    deliveryTime: '',
    loading: false,
    index: '',
    nome: '',
    costumerId: '',
    cliente_id: '',
    customersS: [],
    quantidade: '',
    valor_unitario: '',
    desconto: '',
    pedido_id: '',
    produto_id: '',
    forma_pagamento: '',
    status: '',
  };
  updatePicker = nome => {
    this.setState({ nome: nome });
  };

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    console.log('A date has been picked: ', date);
    this._hideDateTimePicker();
  };

  _showTimePicker = () => this.setState({ isTimePickerVisible: true });

  _hideTimePicker = () => this.setState({ isTimePickerVisible: false });

  _handleTimePicked = time => {
    console.log('A date has been picked: ', time);
    this._hideTimePicker();
  };

  createOrder = () => {
    const {
      total,
      status,
      criado_em,
      cliente_id,
      cadastrante_id,
      forma_pagamento,
      data_prevista,
      data_entrega,
    } = this.state;
    const { setLoading, showToast } = this.props;

    if (this.state.cliente_id) {
      setLoading(true);
      axios
        .post('https://agile-hollows-47291.herokuapp.com/api/v1/pedido', {
          total: '0.00',
          status: '',
          criado_em: '2018-11-22 19:10:25',
          cliente_id: this.state.cliente_id,
          cadastrante_id: 1,
          forma_pagamento: '',
          data_prevista: '2018-12-03',
          data_entrega: '2018-11-22 19:10:25',
        })
        .then(response => {
          this.setState({
            total: '',
            status: '',
            criado_em: '',
            forma_pagamento: '',
            data_prevista: '',
            data_entrega: '',
            response: response.data.dataPedido,
            pedido_id: response.data.dataPedido.id,
          });
          showToast({
            message: 'Selecione os Itens do Pedido',
            type: `success`,
          });
        })
        .catch()
        .then(() => setLoading(false));
    } else {
      console.log(this.state.cliente_id)
    }
  };

  updateOrder = () => {
    const {
      total,
      status,
      criado_em,
      cliente_id,
      cadastrante_id,
      forma_pagamento,
      data_prevista,
      data_entrega,
    } = this.state;
    const { setLoading, showToast } = this.props;

    if (this.state.status) {
      setLoading(true);
      axios
        .put(`https://agile-hollows-47291.herokuapp.com/api/v1/pedido/${this.state.pedido_id}`, {
          total: '0.00',
          status: this.state.status,
          criado_em: '2018-11-22 19:10:25',
          cliente_id: this.state.cliente_id,
          cadastrante_id: 1,
          forma_pagamento: this.state.forma_pagamento,
          data_prevista: '2018-12-03',
          data_entrega: '2018-11-22 19:10:25',
        })
        .then(response => {
          this.setState({
            total: '',
            status: '',
            criado_em: '',
            cliente_id: '',
            cadastrante_id: '',
            forma_pagamento: '',
            data_prevista: '',
            data_entrega: '',
          });
          showToast({
            message: 'Pedido Criado com Sucesso!',
            type: `success`,
          });
        })
        .catch()
        .then(() => setLoading(false));
    } else {
      showToast({ message: 'Erro ao Criar Pedido', type: `error` });
    }
  };


  createOrderItem = () => {
    const {
      quantidade,
      valor_unitario,
      desconto,
      produto_id,
      pedido_id,

    } = this.state;
    const { setLoading, showToast } = this.props;

    if (pedido_id) {
      setLoading(true);
      axios
        .post('https://agile-hollows-47291.herokuapp.com/api/v1/pedido_item', {
          quantidade: this.state.amount,
          valor_unitario: this.state.selectedProduct.preco,
          desconto: 0.00,
          pedido_id: this.state.pedido_id,
          produto_id: this.state.selectedProduct.id,
        })
        .then(response => {
          this.setState({
            quantidade: '',
            valor_unitario: '',
            desconto: '',
            produto_id: '',

          });
          console.log('DEU BOA')
        })
        .catch(() => {})
        .then(() => setLoading(false));
    } else {

    }
  };



  getProducts = () =>
    axios.get('https://agile-hollows-47291.herokuapp.com/api/v1/produto')


  getCostumers = () =>
    axios.get('https://agile-hollows-47291.herokuapp.com/api/v1/cliente')


  componentDidMount() {
    const { setLoading, showToast } = this.props;
    setLoading(true);
    Promise.all([this.getProducts(), this.getCostumers()])
      .then(([{ data: productsData }, { data: costumersData }]) =>
        this.setState({ products: productsData.data, costumers: costumersData.data }))
      .catch()
      .then(() => setLoading(false));
  }


  renderProducts = ({ item: { product } }) => (
    <View style={{display: `flex`, flex: 1}}>
      <ListItem>
      <ProductImage source={{ uri: `${product.imagem}` } || noImage} />
        <ProductDetails>
          <Text>
            {product.marca}
          </Text>

          <Text>
            {product.nome}
          </Text>

          <Text>
          {string.Products.price}
            {product.preco}
          </Text>

          <Text>
          {string.Order.amount}
            {product.amount}
          </Text>

          <Text>
          {string.Order.discount}
            {product.discount}
          </Text>
        </ProductDetails>
      </ListItem>
    </View>
  )





  renderCostumerPicker = () => {
    return (
      <Pick
        selectedValue={this.state.selectedCostumer}
        onValueChange={selectedCostumer => this.setState({ selectedCostumer, cliente_id: selectedCostumer }, () => this.createOrder())}
      >
        <Pick.Item label="Selecione o cliente" value={0} />
        {this.state.costumers.map(({ id, nome }) => {
          return (
            <Pick.Item value={id} label={nome} key={id} />
          );
        })}
      </Pick>
    );
  }

  handleAddProduct = () => {
    const { selectedProducts, selectedProduct, amount, discount } = this.state;

    this.setState({
      selectedProducts: [...selectedProducts, {
        product: { ...selectedProduct, discount, amount }
      }],
      selectedProduct: '',
      amount: '',
      discount: '',
      modalVisible: false,
    }),
      this.createOrderItem();
  }

  render() {
    const { selectedCostumer, modalVisible, products, selectedProduct, amount, discount, selectedProducts } = this.state;
    const { navigation } = this.props;
    return (
      <Background>
        <ScrollWrapper>
          <Label>
            Cliente:
          </Label>
          <View style={{ display: `flex`, flex: 1, flexDirection: `column` }}>

            {this.renderCostumerPicker()}
          </View>
          <Modal
            visible={modalVisible}
            products={products}
            selectedProduct={selectedProduct}
            amount={amount}
            discount={discount}
            handleAddProduct={this.handleAddProduct}
            handleCloseModal={() => this.setState({ modalVisible: false })}
            handleChangeAmount={amount => this.setState({ amount })}
            handleChangeDiscount={discount => this.setState({ discount })}
            handleSelectProduct={selectedProduct => () => this.setState({ selectedProduct })}
          />
          <View style={{ display: `flex`, flex: 8 }}>
            {selectedCostumer ?
              <React.Fragment>
                <View style={{ display: 'flex', flex: 2 }}>

                  <Button
                    title="Adicionar produto"
                    onPress={() => this.setState({ modalVisible: true }, () => this.createOrderItem())}
                  />

                  {selectedProducts.length > 0 ?

                    <FlatList
                      data={selectedProducts}
                      renderItem={this.renderProducts}
                      ItemSeparatorComponent={() => <ListSeparator />}
                    />
                    :
                    <Text>Nenhum produto selecionado</Text>
                  }
                </View>


                <Wrapper>

                  <Button
                    title={string.Order.deliveryDate}
                    onPress={this._showDateTimePicker}
                  />
                  <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                  />

                </Wrapper>
                <Card>
                  <TextWrapper>
                    <Pick
                      selectedValue={this.state.forma_pagamento}
                      onValueChange={(itemValue, itemIndex) => this.setState({ forma_pagamento: itemValue })}>
                      <Pick.Item label="Forma de Pagamento" value="unidade" />
                      <Pick.Item label="Dinheiro" value="Dinheiro" />
                      <Pick.Item label="Cartão de Débito" value="Cartão de Débito" />
                      <Pick.Item label="Cartão de Crédito" value="Cartão de Crédito" />
                      <Pick.Item label="Boleto Bancário" value="Boleto Bancário" />
                    </Pick>
                  </TextWrapper>
                </Card>
                <Card>
                  <TextWrapper>
                    <Pick
                      selectedValue={this.state.status}
                      onValueChange={(itemValue, itemIndex) => this.setState({ status: itemValue })}>
                      <Pick.Item label="Status" value="Status" />
                      <Pick.Item label="Aguardando Entrega" value="Aguardando Entrega" />
                      <Pick.Item label="Aguardando Estoque" value="Aguardando Estoque" />
                      <Pick.Item label="Saiu Para Entrega" value="Saiu Para Entrega" />
                      <Pick.Item label="Entregue" value="Entregue" />
                    </Pick>
                  </TextWrapper>
                </Card>
                <Wrapper>
                  <Button
                    primary
                    title={string.Products.save}
                    onPress={() => this.updateOrder(navigation.navigate('OrderList'))}
                  />
                </Wrapper>
              </React.Fragment>
              : <View />
            }
          </View>
        </ScrollWrapper>
      </Background>
    );
  }
}

export default WithFeedBack(OrderNew);
