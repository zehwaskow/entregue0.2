import React, { Fragment } from "react"
import { Modal, Text, FlatList, View, TouchableHighlight } from 'react-native';
import { TextWrapper, TextField, Pick, Wrapper, Label, ListSeparator, ProductImage, ListItem, ProductDetails } from '@Styles';
import string from '@Strings';
import Button from '../Button';
import { ScrollWrapper } from "../../styles";

const noImage = require ('@Image/no_image.jpg');

export default ({
  products,
  selectedProduct,
  amount,
  discount,
  visible,
  handleCloseModal,
  handleChangeAmount,
  handleChangeDiscount,
  handleSelectProduct,
  handleAddProduct,
}) => {
  const renderItem = ({ item }) => (
    <ListItem>
      <ProductImage source={{uri: `${item.imagem}`} || noImage} />
      
      <ProductDetails>
        <Text>
          {item.marca}
        </Text>

        <Text>
          {item.nome}
        </Text>

        <Text>
          {item.preco}
        </Text>
      </ProductDetails>

    </ListItem>
  )

  const renderProducts = ({ item }) => (
    <TouchableHighlight underlayColor = {'white'} onPress={handleSelectProduct(item)}>
      {renderItem({item})}
    </TouchableHighlight>
  )

  return (
    
    <Modal
      animationType="slide"
      transparent={false}
      visible={visible}      
    >
    <ScrollWrapper>
      <View style={{ marginTop: 22, padding: 16, display: `flex`, flex: 1 }}>
        <View style={{display: `flex`, flex: 4}}>
          <Label>
            Produto:
          </Label>
          {
            !selectedProduct ?
              <FlatList
                data={products}
                renderItem={renderProducts}
                ItemSeparatorComponent={() => <ListSeparator />}
              />
              :
              <Fragment>               
                {renderItem({item: selectedProduct})}
                <View style={{display: `flex`, flex: 4, marginTop: 10}}>                
                  <Label>
                    Informe a quantidade:
                </Label>

                  <TextWrapper>
                    <TextField
                      placeholder={string.Order.amount}
                      value={amount}
                      keyboardType={'numeric'}
                      onChangeText={handleChangeAmount}
                    />
                  </TextWrapper>

                  <Label>
                    Informe o desconto:
                  </Label>

                  <TextWrapper>
                    <TextField
                      placeholder={string.Order.discount}
                      value={discount}
                      keyboardType={'numeric'}                      
                      onChangeText={handleChangeDiscount}
                    />
                  </TextWrapper>
                </View>
              </Fragment>
          }


          </View>
          <Wrapper>
            <Button
              title="Cancelar"
              onPress={handleCloseModal}
              />

              <Button
              primary
              title="Salvar"
              onPress={handleAddProduct}
              />
          </Wrapper>

      </View>
      </ScrollWrapper>
    </Modal>
    
  )
}

