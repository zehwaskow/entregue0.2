import React, { Component } from 'react';
import {createStackNavigator, createSwitchNavigator, createBottomTabNavigator} from 'react-navigation';
import Login from '@Components/Login';
import SignUp from '@Components/SignUp';
import CustomerNew from '@Components/CustomerNew';
import ForgotPassword from '@Components/ForgotPassword';
import UserNew from '@Components/UserNew';
import ProductNew from '@Components/ProductNew';
import OrderNew from '@Components/OrderNew';
import OrderList from '@Components/OrderList';
import OrderUpdate from '@Components/OrderUpdate';
import DateTimePickerTester from '@Components/DateTimePickerTester';
import ProductsList from '@Components/ProductsList';
import TestScreen from '@Components/TestScreen';
import CustomerList from '@Components/CustomerList';
import ProductUpdate from '@Components/ProductUpdate';
import CustomerUpdate from '@Components/CustomerUpdate';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Strings from '@Strings';
import Colors from '@Styles/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';

console.disableYellowBox = true;

const Auth = createStackNavigator (
  {   
    Login: {
      screen: Login,
      navigationOptions: () => ({
        header: null,
      }),
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: {
        title: Strings.Login.register,
      },
    },
    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: {
        title: Strings.Login.forgotPassword,
      },
    },
  },
  {
    initialRoute: Login,
    navigationOptions: {
      headerStyle: {
        backgroundColor: Colors.header,
      },
      headerTintColor: Colors.headerText,
    },
  }
);

const ProductStack = createStackNavigator({
  ProductsList: {
    screen: ProductsList,
    navigationOptions: {
      title: `Produtos`,
    },
  },
  ProductNew: {
    screen: ProductNew,
    navigationOptions: {
      title: `Cadastrar Produto`,
    },
  },
  ProductUpdate: {
    screen:  ProductUpdate,
    navigationOptions: {
      title: `Atualizar Produto`,
    },
  },
})

const CustomerStack = createStackNavigator({
  CustomerList: {
    screen: CustomerList,
    navigationOptions: {
      title: `Clientes`,
    },
  },
  CustomerNew: {
    screen: CustomerNew,
    navigationOptions: {
      title: `Criar Cliente`,
    },
  },
  CustomerUpdate: {
    screen: CustomerUpdate,
    navigationOptions: {
      title: `Atualizar Cliente`,
    },
  },
})

const OrderStack = createStackNavigator({
  OrderList: {
    screen: OrderList,
    navigationOptions: {
      title: `Pedidos`,
    },
  },
  OrderNew: {
    screen: OrderNew,
    navigationOptions: {
      title: `Criar Pedido`,
    },
  },
  OrderUpdate: {
    screen: OrderUpdate,
    navigationOptions: {
      title: `Atualizar Pedido`,
    },
  },

})

const getIconName = routeName => {
  if(routeName === 'ProductStack'){
    return `ios-cube`
  }
  if(routeName === 'CustomerStack'){
    return `ios-contacts`
  }
  
  return `ios-cart`;  
}

const bottomNavigatior  = createBottomTabNavigator (
  {
    ProductStack: {
      screen: ProductStack,
      navigationOptions: {
        title: `Produtos`,
      },
    },
    CustomerStack: {
      screen: CustomerStack,
      navigationOptions: {
        title: `Clientes`,
      },
    },
    OrderStack:  {
      screen: OrderStack,
      navigationOptions: {
        title: `Pedido`,
      },
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        const iconName = getIconName(routeName);        
        return <Ionicons name={iconName} size={24} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray'
    },
  }
);

export default createSwitchNavigator (
  {
    Auth: {
      screen: Auth,
      navigationOptions: () => ({
        header: null,
      }),
    },
    App: bottomNavigatior,
  },
  {
    initialRoute: `Loading`,
  }
);

